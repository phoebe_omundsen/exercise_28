using System;

namespace exercise_28
{
    class Person
    {
        protected string Name; //Class Person
        protected int Age;

        public Person(string _name, int _age) //Constructor
        {
            Name = _name;
            Age = _age;
        }
        public void sayHello()
        {
            Console.WriteLine($"Hello my name is {Name} and I am {Age} years old!");
        }
    }
    }

