﻿using System;
using System.Collections.Generic;

namespace exercise_28
{
    class Program
    {
        static List<string> papers = new List<string> {"Comp5002", "Comp5004", "Comp5008", "Inft5001"};

        static void Main(string[] args) //Main Method Class Program
        {
            var s1 = new Student("Kyle", 25, 879500);

            s1.sayHello();

            s1.ListAllPapers(papers);

            s1.PrintFavouritePaper(papers[1]);

            var s2 = new Student("Jess", 14, 879400);

            s2.sayHello();

            s2.ListAllPapers(papers);

            s2.PrintFavouritePaper(papers[0]);
            
            var t1 = new Teacher("Joy", 47, 1234560, papers[0]);

            t1.sayHello();
        }
    }
}
