using System.Collections.Generic;
using System;

namespace exercise_28
{
    class Student : Person
        {
            private int StudentId; //Class Student

            public Student(string _name, int _age, int _studentId) : base (_name, _age)
            {
                StudentId = _studentId; //Constructor
            }

            public void ListAllPapers(List<string> papers)
            {
                var output = string.Join(", ", papers);

                Console.WriteLine($"I take the following papers; {output}");
            }

            public void PrintFavouritePaper(string favpaper)
            {
                Console.WriteLine($"My Favourite paper is {favpaper}");
            }
        }
    }

