using System;
using System.Collections.Generic;

namespace exercise_28
{
    class Teacher : Person
    {
        private int TeacherId;
        private string Paper;

        public Teacher(string _name, int _age, int _teacherId, string _paper) : base (_name, _age)
        {
            TeacherId = _teacherId;
            Paper = _paper;
        }

        public void ListPaper()
        {
            Console.WriteLine($"{Name} teaches {Paper}.");
        }
    }
    }

